<!doctype html>
<html>
<head>
    <title>WWi gallery</title>
    <link href="style.css" type="text/css" rel="stylesheet">
</head>
<body>
<div class="slider-holder">
    <?php
    for ($i=1; $i<=3 ; $i++){
        echo("<span id=\"slider-image-".$i."\"></span>");
    }
    ?>
    <div class="image-holder">
        <?php
        for ($i=1; $i<=3 ; $i++){
            echo("<img src=\"".$i.".jpg\" class=\"slider-image\" /><div class=\"top-left".$i."\">Oud&Nieuw</div>");
        }
        ?>
    </div>
    <div class="button-holder">
        <?php
        for ($i=1; $i<=3 ; $i++){
            echo("<a href=\"#slider-image-".$i."\" class=\"slider-change\"></a> ");
        }
        ?>
    </div>
</div>
</body>
</html>

